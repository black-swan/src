/* USER CODE BEGIN 0 */
typedef enum
{
	Display_State,
	Idle_State
} eSystemState;

eSystemState DisplayHandler(void)
{
    HAL_ADC_Start(&hadc1);
        HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
        int raw = HAL_ADC_GetValue(&hadc1);
		char msg[20];

    if(HAL_GPIO_ReadPin(GPIOD, CLEAR_BUTTON_Pin))
          {
              if(raw >= 500 && raw <=3750) // 0.4V to 3V
                            HAL_GPIO_WritePin(GPIOD,GREEN_LED_Pin,GPIO_PIN_SET);
							sprintf(msg, "UVB present\r\n");
							HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), HAL_MAX_DELAY);
							
              else
                            HAL_GPIO_WritePin(GPIOD,RED_LED_Pin,GPIO_PIN_SET);
							sprintf(msg, "UVB not present");
		  					HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), HAL_MAX_DELAY);

     	  } else {

          }

         return Idle_State;
}

eSystemState IdleHandler(void)
{
	//code goes here
	HAL_GPIO_WritePin(GPIOD,RED_LED_Pin,GPIO_PIN_RESET); //red
	HAL_GPIO_WritePin(GPIOD,GREEN_LED_Pin,GPIO_PIN_RESET); //green
	HAL_GPIO_WritePin(GPIOD,BLUE_LED_Pin,GPIO_PIN_RESET); //blue
	HAL_Delay(100);
	return Display_State;
}

/* USER CODE END 0 */

  while (1)
  {
    /* USER CODE END WHILE */

	    /* USER CODE BEGIN 3 */
		  switch(eNextState)
		  {
		  case Idle_State:
			  eNextState = IdleHandler();
			  break;
		  case Display_State:
			  eNextState = DisplayHandler();
			  break;
		  default:
			  eNextState = IdleHandler();
			  break;
		  }
	  }
	  /* USER CODE END 3 */
}